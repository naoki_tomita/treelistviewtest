﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using TreeListViewTest.Controls;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Documents;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;

// テンプレート コントロールのアイテム テンプレートについては、http://go.microsoft.com/fwlink/?LinkId=234235 を参照してください

namespace TreeListViewTest
{
    public sealed class DoubleValueBuffer : ValueBuffer<double>
    {
        public DoubleValueBuffer()
        {
            this.DefaultStyleKey = typeof(DoubleValueBuffer);
        }
    }
}
