﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Reactive.Subjects;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using System.Reactive.Linq;
using WinRTXamlToolkit.Controls.Extensions;
using TreeListViewTest.Extensions;
using Windows.Foundation;


namespace TreeListViewTest.Behaviors
{
    //http://stackoverflow.com/questions/8480252/is-there-a-simple-way-to-have-a-listview
    //-automatically-scroll-to-the-most-recent

    //http://pmichaels.net/2014/10/31/automatically-focus-on-the-newest-item-in-a-listview
    //-using-winrt-mvvm-and-behaviors-sdk/


    public sealed class AutoScrollListViewBehavior : Behaviour<ListView>
    {
        // Need to track whether we've attached to the collection changed event
        //bool _collectionChangedSubscribed = false;

        private Subject<object> _scroller;
        /// <summary>
        /// スクロール要求を処理
        /// </summary>
        private Subject<object> Scroller
        {
            get
            {
                if (this._scroller == null)
                {
                    this._scroller = new Subject<object>();
                }
                return this._scroller;
            }
        }

        private Dictionary<string, IDisposable> _unsubscrivers;
        /// <summary>
        /// オブザーバの登録解除用オブジェクトを保持
        /// </summary>
        private Dictionary<string, IDisposable> Unsubscrivers
        {
            get
            {
                if (this._unsubscrivers == null)
                {
                    this._unsubscrivers = new Dictionary<string, IDisposable>();
                }
                return this._unsubscrivers;
            }
        }

        /// <summary>
        /// ListView内部のScrollViewer
        /// </summary>
        private ScrollViewer scrollViewer;

        private double _waitTime = 80;
        /// <summary>
        /// 最後に要素が追加されてからスクロールを開始するまでの待機時間[millisecond]
        /// </summary>
        public double WaitTime
        {
            get { return this._waitTime; }
            set
            {
                if (this._waitTime != value)
                {
                    this._waitTime = value;
                }
            }
        }
        

        /// <summary>
        /// アタッチ時の初期化処理
        /// </summary>
        protected override void OnAttached()
        {
            base.OnAttached();

            // 要素の追加が一定時間行われなくなるまでの履歴を保存
            this.Scroller
                .Buffer(this.Scroller.Throttle(TimeSpan.FromMilliseconds(this.WaitTime)))
                .ObserveOn(SynchronizationContext.Current)
                .Subscribe(x => ScrollIntoViewMain(x))
                .AddDisposable(this.Unsubscrivers, "Scroller");


            // 要素が選択されたら、その要素までスクロール
            Observable.FromEvent
                <SelectionChangedEventHandler, SelectionChangedEventArgs>(
                h => (sender, e) => h(e),
                h => AssociatedObject.SelectionChanged += h,
                h => AssociatedObject.SelectionChanged -= h)
                .Subscribe(e =>
                {
                    if (e.AddedItems != null && e.AddedItems.Count > 0)
                    {
                        ScrollIntoView(e.AddedItems.Last());
                    }
                })
                .AddDisposable(this.Unsubscrivers, "SelectionChanged");

            //AssociatedObject.SelectionChanged += SelectionChanged;

            // The ItemSource of the listView will not be set yet, 
            // so get a method that we can hook up to later

            // DataContextが変更されたときの動作を登録
            Observable.FromEvent
                <TypedEventHandler<FrameworkElement, DataContextChangedEventArgs>,
                DataContextChangedEventArgs>(
                h => (sender, e) => h(e),
                h => AssociatedObject.DataContextChanged += h,
                h => AssociatedObject.DataContextChanged -= h)
                .Subscribe(e => DataContextChanged(e))
                .AddDisposable(this.Unsubscrivers, "DataContextChanged");

            //AssociatedObject.DataContextChanged += DataContextChanged;

            // ListViewの初期化終了時に内部のScrollViewerへの参照を保持
            Observable.FromEvent<RoutedEventHandler, RoutedEventArgs>(
               h => (sender, e) => h(e),
               h => AssociatedObject.Loaded += h,
               h => AssociatedObject.Loaded -= h)
               .Subscribe(e => this.scrollViewer =
                   AssociatedObject.GetFirstDescendantOfType<ScrollViewer>())
               .AddDisposable(this.Unsubscrivers, "Loaded");

            //AssociatedObject.Loaded += (o, e) =>
            //{
            //    this.scrollViewer = AssociatedObject.GetFirstDescendantOfType<ScrollViewer>();
            //};
        }

        //private void SelectionChanged(SelectionChangedEventArgs e)//object sender, SelectionChangedEventArgs e)
        //{
        //    if (e.AddedItems != null && e.AddedItems.Count > 0)
        //    {
        //        var item = e.AddedItems.Last();
        //        ScrollIntoView(item);
        //    }
        //}
        //
        //private void CollectionChanged(NotifyCollectionChangedEventArgs e)//object sender, )
        //{
        //    if (e.NewItems != null && e.NewItems.Count > 0)
        //    {
        //        var item = e.NewItems[e.NewItems.Count - 1];//.Last();
        //        ScrollIntoView(item);
        //    }
        //}

        /// <summary>
        /// DataContextが変更されたとき、ItemsSourceの変更を監視するオブザーバを登録
        /// </summary>
        /// <param name="args"></param>
        private void DataContextChanged(DataContextChangedEventArgs args)//FrameworkElement sender, 
        {
            // The ObservableCollection implements the INotifyCollectionChanged interface
            // However, if this is bound to something that doesn't then just don't hook the event
            var collection = AssociatedObject.ItemsSource as INotifyCollectionChanged;
            if (collection == null)
            {
                return;
            }


            // The data context has been changed, so now hook 
            // into the collection changed event

            // ItemsSourceに新しい要素が追加されたら、その要素までスクロール
            Observable.FromEvent
                <NotifyCollectionChangedEventHandler, NotifyCollectionChangedEventArgs>(
                h => (sender, e) => h(e),
                h => collection.CollectionChanged += h,
                h => collection.CollectionChanged -= h)
                .Subscribe(e =>
                {
                    if (e.NewItems != null && e.NewItems.Count > 0)
                    {
                        ScrollIntoView(e.NewItems[e.NewItems.Count - 1]);
                    }
                })
                .AddDisposable(this.Unsubscrivers, "CollectionChanged");


            //if (collection != null && !_collectionChangedSubscribed)
            //{
            //    collection.CollectionChanged += CollectionChanged;
            //    _collectionChangedSubscribed = true;
            //}

        }

        /// <summary>
        /// 指定した要素へのスクロールを要請
        /// </summary>
        /// <param name="item"></param>
        private void ScrollIntoView(object item)
        {
            this.Scroller.OnNext(item);

            //int count = AssociatedObject.Items.Count;
            //if (count > 0)
            //{
            //    var last = AssociatedObject.Items[count - 1];
            //    AssociatedObject.ScrollIntoView(last);
            //}
        }

        /// <summary>
        /// 与えられた複数の要素が可能な限り画面内に表示されるようスクロール
        /// </summary>
        /// <param name="list"></param>
        private void ScrollIntoViewMain(IList<object> list)
        {

            // スクロール対象の要素が受け取れない場合、下端までスクロール
            if (list == null || list.Count <= 0)
            {
                int count = AssociatedObject.Items.Count;
                if (count > 0)
                {
                    var last = AssociatedObject.Items[count - 1];
                    AssociatedObject.ScrollIntoView(last);
                }
                return;
            }


            //if (list != null && list.Count > 0)// && AssociatedObject.Items.Contains(item))
            //{

            //var scrollViewer = AssociatedObject.GetFirstDescendantOfType<ScrollViewer>();

            // 内部ScrollViewerが見つからなかった場合は何もしない
            if (this.scrollViewer == null)
            {
                return;
            }

            // スクロール可能な高さが無い場合は何もしない
            if (scrollViewer.ScrollableHeight <= 0.0)
            {
                return;
            }

            //var fullHeight = scrollViewer.ExtentHeight;

            // 現在選択されている要素がある場合はそれもスクロール対象に含める
            var leaderItem = AssociatedObject.SelectedItem;
            if (leaderItem != null)
            {
                list.Add(leaderItem);
            }

            //var count = AssociatedObject.Items.Count;

            // 全要素の高さを取得し、その累積から各要素のScrollViewer上の位置を計算
            var height = Enumerable.Range(0, AssociatedObject.Items.Count)
                .Select(x => AssociatedObject.ContainerFromIndex(x) as ListViewItem)
                .Where(x => x != null)
                .Select(x => x.DesiredSize.Height)
                .Integral()
                .ToList();


            var minOfTop = double.MaxValue;
            var maxOfBottom = double.MinValue;

            // スクロール対象要素全体の上端と下端を求める
            var position = list
                .Where(x => x != null && AssociatedObject.Items.Contains(x))
                .Select(x =>
                {
                    var index = AssociatedObject.Items.IndexOf(x);

                    var Top = index > 0 ? height[index - 1] : 0.0;
                    var Bottom = height[index];

                    if (Top < minOfTop)
                    {
                        minOfTop = Top;
                    }
                    if (Bottom > maxOfBottom)
                    {
                        maxOfBottom = Bottom;
                    }

                    return new { Top, Bottom, };

                }).ToList();

            //var minOfTop = position.Select(x => x.Top).Min();
            //var maxOfBottom = position.Select(x => x.Bottom).Max();
            
            // 表示されている高さ(親ListViewのものと同じ)
            var viewHeight = scrollViewer.ActualHeight;

            // 現在のスクロール位置
            var currentVerticalOffset = scrollViewer.VerticalOffset;

            // 新しいスクロール位置
            var newVerticalOffset = -1.0;

            if (maxOfBottom - minOfTop > viewHeight)
            {
                // スクロール対象要素が全て画面内に入りきらない場合
                // 最後に追加された要素を最優先にする
                var centerItem = position.Last();
                var higherSize = centerItem.Bottom - minOfTop;
                var lowerSize = maxOfBottom - centerItem.Top;
                
                // 最後の要素を下端に付けるか上端に付けるか選択
                if (higherSize > lowerSize)
                {
                    newVerticalOffset = centerItem.Bottom - viewHeight;
                }
                else
                {
                    newVerticalOffset = centerItem.Top;
                }

                // 負のオフセットは上端へのスクロール扱い
                if (newVerticalOffset < 0.0)
                {
                    newVerticalOffset = 0.0;
                }

                //minOfTop = position.Last().Top;
                //maxOfBottom = position.Last().Bottom;
            }
            else if (maxOfBottom > viewHeight + currentVerticalOffset)
            {
                // 現在の位置より下へスクロール
                newVerticalOffset = maxOfBottom - viewHeight;
            }
            else if (minOfTop < currentVerticalOffset)
            {
                // 現在の位置より上へスクロール
                newVerticalOffset = minOfTop;
            }

            // スクロール開始
            if (newVerticalOffset >= 0.0)
            {
                scrollViewer.ChangeView(null, newVerticalOffset, null);
            }

            return;
        }

        /// <summary>
        /// デタッチ時の解放処理
        /// </summary>
        protected override void OnDetaching()
        {
            base.OnDetaching();

            // 登録されたオブザーバを全て解除
            this.Unsubscrivers.ForEach(x => x.Value.Dispose());
            this.Unsubscrivers.Clear();

            //AssociatedObject.SelectionChanged -= SelectionChanged;
            //AssociatedObject.DataContextChanged -= DataContextChanged;

            // Detach from the collection changed event
            //var collection = AssociatedObject.ItemsSource as INotifyCollectionChanged;
            //if (collection != null && _collectionChangedSubscribed)
            //{
            //    collection.CollectionChanged -= CollectionChanged;
            //    _collectionChangedSubscribed = false;
            //
            //}
        }
    }
}
