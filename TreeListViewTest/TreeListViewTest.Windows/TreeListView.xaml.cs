﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using TreeListViewTest.TreeList;
using TreeListViewTest.Extensions;
using System.Reactive.Disposables;


namespace TreeListViewTest
{
    public sealed partial class TreeListView : UserControl
    {

        private ObservableCollection<ITreeNode> TreeList { get; set; }
        public ListView ListView { get { return this.listView; } }
        private CompositeDisposable unsubscribers = new CompositeDisposable();


        public ITreeNode TreeRoot
        {
            get { return (ITreeNode)GetValue(TreeRootProperty); }
            set { SetValue(TreeRootProperty, value); }
        }

        public static readonly DependencyProperty TreeRootProperty =
          DependencyProperty.Register("TreeRoot", typeof(ITreeNode), typeof(TreeListView),
            new PropertyMetadata(null, new PropertyChangedCallback(OnTreeRootChanged)));

        private static void OnTreeRootChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var thisInstance = d as TreeListView;
            var treeRoot = e.NewValue as ITreeNode;

            thisInstance.ChangeTreeRoot(treeRoot);
        }



        public DataTemplate InnerTemplate
        {
            get { return (DataTemplate)GetValue(InnerTemplateProperty); }
            set { SetValue(InnerTemplateProperty, value); }
        }
        public static readonly DependencyProperty InnerTemplateProperty =
          DependencyProperty.Register("InnerTemplate", typeof(DataTemplate),
          typeof(TreeListView), new PropertyMetadata(null));
        

        public double IndentWidth
        {
            get { return (double)GetValue(IndentWidthProperty); }
            set { SetValue(IndentWidthProperty, value); }
        }
        public static readonly DependencyProperty IndentWidthProperty =
            DependencyProperty.Register("IndentWidth", typeof(double),
            typeof(TreeListView), new PropertyMetadata(0.0));


        public double OpenerWidth
        {
            get { return (double)GetValue(OpenerWidthProperty); }
            set { SetValue(OpenerWidthProperty, value); }
        }
        public static readonly DependencyProperty OpenerWidthProperty =
            DependencyProperty.Register("OpenerWidth", typeof(double),
            typeof(TreeListView), new PropertyMetadata(0.0));




        public TreeListView()
        {
            this.InitializeComponent();
        }


        private void ChangeTreeRoot(ITreeNode treeRoot)
        {
            if (this.unsubscribers != null)
            {
                this.unsubscribers.Dispose();
            }
            this.unsubscribers = new CompositeDisposable();

            treeRoot.TreeController.Subscribe(i =>
            {
                var newList = this.SerializeTree(this.TreeRoot, 0);
                this.TreeList.Imitate(newList, (x, y) => Object.ReferenceEquals(x, y));
            }).AddDisposable(this.unsubscribers);

            //Set TreeList
            this.TreeList = new ObservableCollection<ITreeNode>(this.SerializeTree(treeRoot, 0));

            this.listView.ItemsSource = this.TreeList;
        }

        private IEnumerable<ITreeNode> SerializeTree(ITreeNode treeRoot, int rootHierarchy)
        {
            return treeRoot.TreeController.GetAllChildren()
                .Where(x => x.TreeController.Visible);
        }


        private void IndentButton_Tapped(object sender, TappedRoutedEventArgs e)
        {
            var button = (FrameworkElement)sender;
            var vm = button.DataContext as ITreeNode;

            this.listView.SelectedItem = null;//reset
            this.listView.SelectedItem = vm.Parent;
        }
        
        private void OpenButton_Tapped(object sender, TappedRoutedEventArgs e)
        {
            var button = (FrameworkElement)sender;
            var vm = button.DataContext as ITreeNode;

            this.listView.SelectedItem = vm;

            if (vm.TreeController.IsOpen)
            {
                vm.TreeController.Close();
            }
            else
            {
                vm.TreeController.Open();
            }
        }
        
        public bool SelectItem(ITreeNode item)
        {
            if (item != null && this.TreeList.Contains(item))
            {
                this.ListView.SelectedItem = item;
                return true;
            }
            this.ListView.SelectedItem = null;
            return false;
        }
    }
}
