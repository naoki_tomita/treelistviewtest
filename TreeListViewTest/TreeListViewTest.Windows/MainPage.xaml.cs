﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using TreeListViewTest.ViewModel;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using TreeListViewTest.Behaviors;
using System.Threading.Tasks;
using TreeListViewTest.DataModel;
using TreeListViewTest.Controls;

// 空白ページのアイテム テンプレートについては、http://go.microsoft.com/fwlink/?LinkId=234238 を参照してください

namespace TreeListViewTest
{
    /// <summary>
    /// Frame 内へナビゲートするために利用する空欄ページ。
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private MainPageViewModel viewModel;

        public MainPage()
        {
            this.InitializeComponent();
            this.viewModel = (MainPageViewModel)this.DataContext;
        }

        private void AppBarButton_Tapped(object sender, TappedRoutedEventArgs e)
        {
            var button = (FrameworkElement)sender;
            var vm = button.DataContext as PersonViewModel;

            this.list.SelectItem(vm);
            vm.AddTestItem();
        }


        private void AppBarButton_Tapped_2(object sender, TappedRoutedEventArgs e)
        {
            var button = (FrameworkElement)sender;
            var vm = button.DataContext as PersonViewModel;

            this.list.SelectItem(null);
            vm.RemoveSelf();

        }


        private void list_DataContextChanged(FrameworkElement sender, DataContextChangedEventArgs args)
        {
            var behavior = new AutoScrollListViewBehavior();
            behavior.Attach(((TreeListView)sender).ListView);
        }

        private void AppBarButton_Tapped_1(object sender, TappedRoutedEventArgs e)
        {
            var button = (FrameworkElement)sender;
            var vm = button.DataContext as PersonViewModel;

            this.list.SelectItem(null);
            vm.DecorationVisible.Value = !vm.DecorationVisible.Value;
            this.list.SelectItem(vm);
        }
    }
}
