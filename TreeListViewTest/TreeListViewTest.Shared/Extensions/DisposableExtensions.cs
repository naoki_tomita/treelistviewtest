﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TreeListViewTest.Extensions
{
    public static class DisposableExtensions
    {
        public static void AddDisposable(this IDisposable disposable, ICollection<IDisposable> list)
        {
            list.Add(disposable);
        }

        public static void AddDisposable<T>
            (this IDisposable disposable, IDictionary<T, IDisposable> dictionary, T key)
        {
            IDisposable result;
            if(dictionary.TryGetValue(key, out result)){
                result.Dispose();
                dictionary.Remove(key);
            }
            //if (dictionary.ContainsKey(key))
            //{
            //    dictionary[key].Dispose();
            //    dictionary.Remove(key);
            //}
            dictionary.Add(key, disposable);
        }

        //public static ToOvservable()
    }
}
