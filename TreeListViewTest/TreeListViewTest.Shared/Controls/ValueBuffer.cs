﻿using System;
using System.Collections.Generic;
using System.Text;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using System.Windows;

namespace TreeListViewTest.Controls
{
    
    public class ValueBuffer<T> : Control
    {

        public T Input
        {
            get { return (T)GetValue(InputProperty); }
            set { SetValue(InputProperty, value); }
        }

        public static readonly DependencyProperty InputProperty =
            DependencyProperty.Register("Input", typeof(T), typeof(ValueBuffer<T>),
            new PropertyMetadata(default(T), new PropertyChangedCallback(OnInputChanged)));

        private static void OnInputChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var thisInstance = d as ValueBuffer<T>;
            var newValue = (T)e.NewValue;

            thisInstance.Output = newValue;
        }


        public T Output
        {
            get { return (T)GetValue(OutputProperty); }
            set { SetValue(OutputProperty, value); }
        }

        public static readonly DependencyProperty OutputProperty =
            DependencyProperty.Register("Output", typeof(T), typeof(ValueBuffer<T>),
            new PropertyMetadata(default(T), new PropertyChangedCallback(OnOutputChanged)));

        private static void OnOutputChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {

        }

        public ValueBuffer()
        {
            this.DataContextChanged += (o, e) =>
            {
                this.Output = this.Input;
            };
        }
    }

    //public class DoubleValueBuffer : ValueBuffer<double>
    //{
    //}
    //public class IntValueBuffer : ValueBuffer<int>
    //{
    //}
    //public class StringValueBuffer : ValueBuffer<string>
    //{
    //}
}
