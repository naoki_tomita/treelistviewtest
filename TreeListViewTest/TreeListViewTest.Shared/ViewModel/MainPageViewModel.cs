﻿using System;
using System.Collections.Generic;
using System.Text;
using TreeListViewTest.DataModel;
using TreeListViewTest.TreeList;
using System.Linq;
using System.Collections.Specialized;
using System.Collections.ObjectModel;
using TreeListViewTest.Extensions;
using TreeListViewTest.Utility;

namespace TreeListViewTest.ViewModel
{
    public class MainPageViewModel : BindableBase
    {
        private TreeListModel Model { get; set; }

        public PersonViewModel TreeNode { get; private set; }

        public MainPageViewModel()
        {
            this.Model = new TreeListModel();

            this.TreeNode = new PersonViewModel(this.Model.TreeRoot, null);
            
            this.TreeNode.TreeController.Open();
        }
    }
}
