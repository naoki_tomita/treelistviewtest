﻿using System;
using System.Linq;
using Windows.UI.Xaml.Media;
using Reactive.Bindings;
using System.Reactive.Linq;
using TreeListViewTest.DataModel;
using TreeListViewTest.TreeList;
using TreeListViewTest.Extensions;
using Windows.UI.Xaml;


namespace TreeListViewTest.ViewModel
{
    public class PersonViewModel : ITreeNode
    {
        private PersonModel Content { get; set; }
        public ITreeNode Parent { get; private set; }
        public TreeItemController TreeController { get; private set; }


        public ReactiveProperty<string> Name { get; private set; }
        public ReactiveProperty<int> Age { get; private set; }

        private byte[] baseColor = new byte[] { 0xFF, 0xFF, 0x7F, 0x50 };
        public ReactiveProperty<Brush> BackGroundColor { get; private set; }

        public ReactiveProperty<bool> DecorationVisible { get; private set; }
        public ReactiveProperty<Visibility> DecorationVisibility { get; private set; }
        public ReactiveProperty<string> OptionText { get; private set; }

        private static int count = 0;


        public PersonViewModel(PersonModel content, ITreeNode parent)
        {
            this.Content = content;

            this.Content.Children.CollectionChanged += (o, e) =>
            {
                this.RefreshController();
            };

            this.Name = this.Content.Name;
            this.Age = this.Content.Age;

            //this.Age = this.Content.Age
            //    .Select(x => x + 1)
            //    .ToReactiveProperty();
            //
            //this.Age.Select(x => x - 1)
            //    .Subscribe(x => this.Content.Age.Value = x);

            this.DecorationVisible = new ReactiveProperty<bool>(false);
            this.DecorationVisibility = this.DecorationVisible
                .Select(x => x ? Visibility.Visible : Visibility.Collapsed)
                .ToReactiveProperty();
            this.OptionText = this.DecorationVisible
                .Select(x => x ? "option"+DateTime.Now.Second : "e")
                .ToReactiveProperty();


            this.Parent = parent;

            this.TreeController = new TreeItemController(this);

            this.BackGroundColor = this.TreeController.Hierarchy
                .Select(x => this.CalcHierarchialColor(x))
                .ToReactiveProperty();

            this.RefreshController();
        }

        private void RefreshController()
        {
            var removedItems = this.TreeController.Children.Imitate(this.Content.Children,
                (x, y) => object.ReferenceEquals(((PersonViewModel)x).Content,y), 
                y => new PersonViewModel(y,this));

            removedItems.ForEach(x => x.Dispose());

            this.TreeController.Refresh();
        }



        private Brush CalcHierarchialColor(int hierarchy)
        {
            var color = new byte[4];
            var k = hierarchy * 0.2 + 1.0;

            color[0] = baseColor[0];
            for (int i = 1; i < 4; i++)
            {
                var c = baseColor[i] * k;
                if (c > 0xFF)
                {
                    c = 0xFF;
                }
                color[i] = (byte)c;
            }

            return new SolidColorBrush(
                Windows.UI.Color.FromArgb(color[0], color[1], color[2], color[3]));
        }


        public void AddTestItem()
        {
            var item = new PersonModel();
            item.Name.Value = DateTime.Now.ToString();
            item.Age.Value = count++;

            this.AddChild(item);
        }

        public void AddNewItem()
        {
            var item = new PersonModel();
            this.AddChild(item);
        }

        public void AddChild(PersonModel item)
        {
            this.Content.Children.Add(item);
            this.TreeController.Open();
        }

        public bool RemoveChild(PersonModel item)
        {
            return this.Content.Children.Remove(item);
        }

        public void RemoveSelf()
        {
            if (this.Parent != null)
            {
                ((PersonViewModel)this.Parent).RemoveChild(this.Content);
            }
        }



        public void Dispose()
        {
            this.Parent = null;
            this.Content = null;

            this.TreeController.Dispose();
            this.TreeController = null;
        }
    }
}
