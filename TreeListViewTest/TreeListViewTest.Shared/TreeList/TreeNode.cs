﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Collections.Specialized;
using System.Collections.ObjectModel;
using TreeListViewTest.ViewModel;
using TreeListViewTest.DataModel;

namespace TreeListViewTest.TreeList
{
    public interface ITreeNode : IDisposable
    {
        ITreeNode Parent { get; }
        TreeItemController TreeController { get; }
    }
}
