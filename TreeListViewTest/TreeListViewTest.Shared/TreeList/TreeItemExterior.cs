﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Reactive.Subjects;
using System.Reactive.Linq;
using Reactive.Bindings;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using TreeListViewTest.Extensions;
using TreeListViewTest.ViewModel;
using System.Reactive.Disposables;

namespace TreeListViewTest.TreeList
{

    public class TreeItemController : BindableBase, IDisposable, IObservable<TreeItemController>
    {
        public List<ITreeNode> Children { get; private set; }
        public TreeItemController Parent { get; private set; }


        private bool _isOpen;
        public bool IsOpen
        {
            get { return _isOpen; }
            private set
            {
                if (_isOpen != value)
                {
                    _isOpen = value;

                    if (!_isOpen)
                    {
                        this.Children
                            .ForEach(x => x.TreeController.Close());
                    }
                }
            }
        }

        public bool Visible
        {
            get
            {
                return this.Parent != null
                    ? this.Parent.IsOpen
                    : true;
            }
        }


        private SymbolIcon _openerIcon;
        private SymbolIcon OpenerIcon
        {
            get
            {
                if (_openerIcon == null)
                {
                    _openerIcon = new SymbolIcon(Symbol.ShowBcc);
                }
                return _openerIcon;
            }
        }

        private SymbolIcon _closerIcon;
        private SymbolIcon CloserIcon
        {
            get
            {
                if (_closerIcon == null)
                {
                    _closerIcon = new SymbolIcon(Symbol.HideBcc);
                }
                return _closerIcon;
            }
        }


        public IconElement OpenerSymbol
        {
            get { return this.IsOpen ? CloserIcon : OpenerIcon; }
        }

        public Visibility OpenerVisibility
        {
            get
            {
                return this.Children.Count == 0
                    ? Visibility.Collapsed
                    : Visibility.Visible;
            }
        }

        private void ChangeOpenerSymbol()
        {
            RaisePropertyChanged("OpenerSymbol");
            RaisePropertyChanged("OpenerVisibility");
        }

        private Subject<TreeItemController> VisualChanged { get; set; }
        private CompositeDisposable Unsubscribers { get; set; }


        public ReactiveProperty<double> IndentUnitWidth { get; private set; }
        public ReactiveProperty<double> Indent { get; private set; }

        public ReactiveProperty<int> Hierarchy { get; private set; }


        public TreeItemController(ITreeNode content)
        {

            this.Unsubscribers = new CompositeDisposable();
            this.VisualChanged = new Subject<TreeItemController>();


            this.Parent = content.Parent != null
                ? content.Parent.TreeController
                : null;

            this.Children = new List<ITreeNode>();

            this.Hierarchy = new ReactiveProperty<int>(
                Parent != null
                 ? Parent.Hierarchy.Value + 1
                 : 0);

            this.Hierarchy.Subscribe(i =>
            {
                this.Children.ForEach
                    (x => x.TreeController.Hierarchy.Value = i + 1);
            }).AddDisposable(this.Unsubscribers);

            this.IsOpen = false;

            this.IndentUnitWidth = new ReactiveProperty<double>();

            this.Indent = this.IndentUnitWidth
                .CombineLatest(this.Hierarchy, (w, h) => w * (h - 1))
                .ToReactiveProperty();
        }


        public void Refresh()
        {
            this.ChangeVisual(this);
        }


        private void ChangeVisual(TreeItemController sender)
        {
            ChangeOpenerSymbol();

            this.VisualChanged.OnNext(sender);

            if (this.Parent != null)
            {
                this.Parent.ChangeVisual(sender);
            }
        }

        public void Open()
        {
            this.IsOpen = true;
            this.ChangeVisual(this);
        }
        public void Close()
        {
            this.IsOpen = false;
            this.ChangeVisual(this);
        }


        public IEnumerable<ITreeNode> GetAllChildren()
        {
            foreach (var child in this.Children)
            {
                yield return child;

                foreach (var grandchild in child.TreeController.GetAllChildren())
                {
                    yield return grandchild;
                }
            }
        }


        public IDisposable Subscribe(IObserver<TreeItemController> observer)
        {
            var unsubscriver = this.VisualChanged.Subscribe(observer);
            unsubscriver.AddDisposable(this.Unsubscribers);
            return unsubscriver;
        }


        public void Dispose()
        {
            this.Children.ForEach(x => x.Dispose());
            this.Unsubscribers.Dispose();
        }
    }
}
