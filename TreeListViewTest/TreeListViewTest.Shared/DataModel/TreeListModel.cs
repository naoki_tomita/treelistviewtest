﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using TreeListViewTest.ViewModel;

namespace TreeListViewTest.DataModel
{
    public class TreeListModel
    {
        public PersonModel TreeRoot { get; private set; }

        private static int count = 100;

        public TreeListModel()
        {
            this.TreeRoot = new PersonModel();
            this.MakeTestList();
        }

        private void MakeTestList()
        {
            this.TreeRoot.Children.Add(GenerateTestItem());

            this.TreeRoot.Children.Add(GenerateTestItem());
            this.TreeRoot.Children.First().Children.Add(GenerateTestItem());
            this.TreeRoot.Children.First().Children.Add(GenerateTestItem());
            this.TreeRoot.Children.First().Children.First().Children.Add(GenerateTestItem());
             
        }

        private PersonModel GenerateTestItem()
        {
            var item = new PersonModel();
            item.Name.Value = DateTime.Now.ToString();
            item.Age.Value = count++;
            return item;
        }
    }
}
