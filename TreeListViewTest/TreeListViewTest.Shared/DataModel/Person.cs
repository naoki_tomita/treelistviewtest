﻿using System;
using System.Text;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Reactive.Bindings;

namespace TreeListViewTest.DataModel
{

    [DataContract]
    public class PersonModel
    {

        public ObservableCollection<PersonModel> Children { get; private set; }

        [DataMember]
        private List<PersonModel> SavedChildren {
            get { return this.Children.ToList(); }
            set
            {
                this.Children = new ObservableCollection<PersonModel>(value);
            }
        }



        public ReactiveProperty<string> Name { get; private set; }

        [DataMember(Name = "Name")]
        private string SavedName
        {
            get { return this.Name.Value; }
            set
            {
                if (this.Name.Value != value)
                {
                    this.Name.Value = value;
                }
            }
        }

        public ReactiveProperty<int> Age { get; private set; }

        [DataMember(Name = "Age")]
        private int SavedAge
        {
            get { return this.Age.Value; }
            set
            {
                if (this.Age.Value != value)
                {
                    this.Age.Value = value;
                }
            }
        }


        public PersonModel()
        {
            this.Initialize();
        }

        private void Initialize()
        {
            this.Children = new ObservableCollection<PersonModel>();
            this.Name = new ReactiveProperty<string>();
            this.Age = new ReactiveProperty<int>();
        }

        [OnDeserializing]
        private void OnDeserializing(StreamingContext context)
        {
            this.Initialize();
        }

    }
}
